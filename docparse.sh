username=`whoami`
echo "current username: $username"
if [ "$username" != "node" ]; then
   echo "This script must be run as user node. You can do this by running the script via
  sudo su node dispatch.sh" 1>&2
   exit 1
fi
mkdir -p /home/node/apps/docparse/
cp config.json /home/node/apps/docparse/config.json
echo 'docparse config file installed correctly'
