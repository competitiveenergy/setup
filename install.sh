# setup a new ubuntu server to run the docparse system.
# This script is designed to run on a vanilla ubuntu server 12.04.2 LTS 64 bit
# an admin user with username node should already be available on the server with the home diretory /home/node/


echo 'setting up node user'
sudo useradd -d /home/node -s /bin/bash  --comment "Node Web" -m node

echo 'Installing build tools'
sudo apt-get -y update
sudo apt-get -y install build-essential

echo 'Installing Emacs'
sudo apt-get -y install emacs23-nox

echo 'Installing Redis'
sudo add-apt-repository -y ppa:chris-lea/redis-server
sudo apt-get -y update
sudo apt-get install -y redis-server

echo 'Installing node.js'
sudo apt-get -y install python-software-properties python g++ make git
sudo add-apt-repository -y ppa:chris-lea/node.js
sudo apt-get -y update
sudo apt-get -y install nodejs
# allow node.js processes to listen on ports below 1024 without sudo
setcap 'cap_net_bind_service=+ep' /usr/bin/node
# sometimes setcap doesn't link symlinks, execute setcap on the actual node.js binary executable file
setcap 'cap_net_bind_service=+ep' /etc/alternatives/node

sudo npm install -g npm

echo 'installing pdftotext'
sudo apt-get -y install poppler-utils
echo 'Installing couchdb dependencies'
sudo apt-get -y install g++ curl
sudo apt-get -y install erlang-base erlang-dev erlang-eunit erlang-nox
sudo apt-get -y install libmozjs185-dev libicu-dev libcurl4-gnutls-dev libtool

echo 'building couchdb'
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
COUCH_DOWNLOAD_DIR="$CURRENT_DIR/../downloads/couchdb"
mkdir -p $COUCH_DOWNLOAD_DIR
COUCH_DOWNLOAD_PATH="$COUCH_DOWNLOAD_DIR/couchdb.tar.gz"
COUCH_SRC_DIR=$COUCH_DOWNLOAD_DIR/apache-couchdb-1.3.0

wget -O $COUCH_DOWNLOAD_PATH ftp://apache.cs.utah.edu/apache.org/couchdb/source/1.3.0/apache-couchdb-1.3.0.tar.gz
tar -xvzf $COUCH_DOWNLOAD_PATH -C $COUCH_DOWNLOAD_DIR
(cd $COUCH_SRC_DIR && $COUCH_SRC_DIR/configure && make && sudo make install)

echo 'setting up couchdb user'
sudo useradd -M -c "Couchdb Admin" couchdb
sudo chown -R couchdb:couchdb /usr/local/var/log/couchdb
sudo chown -R couchdb:couchdb /usr/local/var/lib/couchdb
sudo chown -R couchdb:couchdb /usr/local/var/run/couchdb

echo 'creating couchdb upstart script'
sudo cp ./couchdb.conf /etc/init/couchdb.conf
sudo chown root:root /etc/init/couchdb.conf

echo 'setting up fleet hub'
sudo npm install -g fleet
sudo cp ./fleet-hub.conf /etc/init/fleet-hub.conf
sudo cp ./fleet-drone.conf /etc/init/fleet-drone.conf

echo 'setting up dispatch'
sudo cp ./dispatch.conf /etc/init/dispatch.conf
sudo npm install -g dispatch-request-spawn-all
sudo cp ./dispatch-request-spawn-all.conf /etc/init/dispatch-request-spawn-all.conf
echo 'setup done'
