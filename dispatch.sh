username=`whoami`
echo "current username: $username"
if [ "$username" != "node" ]; then
   echo "This script must be run as user node. You can do this by running the script via
  sudo su node dispatch.sh" 1>&2
   exit 1
fi
mkdir -p /home/node/apps/dispatch/

# Servers

echo 'cloning bitbucket repos'
git clone git://github.com/nisaacson/dispatcher.git /home/node/apps/dispatch/deploy
git clone git@bitbucket.org:competitiveenergy/router.git /home/node/apps/dispatch/deploy/repos/router
git clone git@bitbucket.org:competitiveenergy/seaport-server.git /home/node/apps/dispatch/deploy/repos/seaport-server
git clone git@bitbucket.org:competitiveenergy/insecure-proxy.git /home/node/apps/dispatch/deploy/repos/insecure-proxy
git clone git@bitbucket.org:competitiveenergy/secure-proxy.git /home/node/apps/dispatch/deploy/repos/secure-proxy
git clone git@bitbucket.org:competitiveenergy/web.git /home/node/apps/dispatch/deploy/repos/web
git clone git@bitbucket.org:competitiveenergy/export-invoices.git /home/node/apps/dispatch/deploy/repos/export-invoices

# Parsers
git clone git@bitbucket.org:competitiveenergy/ngrid-gas-parser.git /home/node/apps/dispatch/deploy/repos/ngrid-gas-parser
git clone git@bitbucket.org:competitiveenergy/ngrid-electric-parser.git /home/node/apps/dispatch/deploy/repos/ngrid-electric-parser
git clone git@bitbucket.org:competitiveenergy/hess-parser.git /home/node/apps/dispatch/deploy/repos/hess-parser
git clone git@bitbucket.org:competitiveenergy/nstar-parser.git /home/node/apps/dispatch/deploy/repos/nstar-parser
git clone git@bitbucket.org:competitiveenergy/parse-old-scraped.git /home/node/apps/dispatch/deploy/repos/parse-old-scraped

# Scrapers
git clone git@bitbucket.org:competitiveenergy/ngrid-gas-scraper.git /home/node/apps/dispatch/deploy/repos/ngrid-gas-scraper
git clone git@bitbucket.org:competitiveenergy/ngrid-electric-scraper.git /home/node/apps/dispatch/deploy/repos/ngrid-electric-scraper
git clone git@bitbucket.org:competitiveenergy/hess-scraper.git /home/node/apps/dispatch/deploy/repos/hess-scraper
git clone git@bitbucket.org:competitiveenergy/nstar-scraper.git /home/node/apps/dispatch/deploy/repos/nstar-scraper
git clone git@bitbucket.org:competitiveenergy/scraper-scheduler.git /home/node/apps/dispatch/deploy/repos/scraper-scheduler



echo 'dispatch repos cloned correctly. You will need to create a dispatch config.json file at /home/node/apps/dispatch/config.json before the dispatch server can start'
