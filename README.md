# Setup
Setup a new ubuntu server to run the docparse system. This script is designed to run on a vanilla ubuntu server 12.04.2 LTS 64 bit. This repo provides an executable `install.sh` script which will perform all the needed setup to get the docparse server up and running

# Usage
To setup a new server, clone this repo and execute the `install.sh` script. When setting up a new server, you will likely need to create a private/public ssh key pair and upload your public key to bitbucket

```bash
sudo apt-get install -y git
git clone https://competitiveenergy@bitbucket.org/competitiveenergy/setup.git`
cd setup/
# install system libraries (node.js, couchdb etc)
./install.sh
# create ssh key for node user
sudo su node ./user.sh
```

After running `user.sh`, there will be a public/private key pair in `/home/node/.ssh`. Copy the contents of the id_rsa.pub file to a new key on bitbucket.

```bash
# print contents of id_rsa.pub
sudo cat /home/node/.ssh/id_rsa.pub
```
Paste the output of the command above to a new key on bitbucket [https://bitbucket.org/account/user/competitiveenergy/ssh-keys/](https://bitbucket.org/account/user/competitiveenergy/ssh-keys/)

# setup dispatch web server (for deploying new code)
After the ssh public key `id_rsa.pub` contents are added to bitbucket, you can setup the Dispatch server

```bash
sudo su node ./dispatch.sh
```

# Configuration
After running the scripts above, you will need to create `config.json` files for both dispatch and DocParse

## DocParse

```bash
sudo su node
mkdir -p ~/downloads
cd ~/downloads
git clone git@bitbucket.org:competitiveenergy/configs.git
cd configs
cp docparseConfig.json /home/node/apps/docparse/config.json
cp dispatchConfig.json /home/node/apps/dispatch/config.json
cp key.pem /home/node/apps/docparse/key.pem
cp cert.pem /home/node/apps/docparse/cert.pem
cd ~/downloads
rm -rf ~/downloads/configs
# leave node user shell
exit
```

The dispatch server provides a web front-end to the fleet hub and drone services. You can access Dispatch at http://docparse.com:4000

# Finalize
After all the services are installed and configuration files are in place, reboot the system.

```bash
sudo reboot
```

wait for system to reboot and log in through ssh again

```bash
# check couchdb
curl localhost:5984

# check redis
redis-cli

# check fleet hub
ps -ef | grep hub

# check fleet drone
ps -ef | grep drone
```


# Clone Database
The couchdb database should be replicated from a remote host to the local database server. See `replicate.sh` for details

```bash
curl  -X POST -H 'Content-Type:application/json' -d '{"source":"http://10.0.2.2:5984/docparse","target":"http://127.0.0.1:5984/docparse","create_target":true}' localhost:5984/_replicate
```
