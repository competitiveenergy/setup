# This script must be run as user node
username=`whoami`
echo "current username: $username"
if [ "$username" != "node" ]; then
   echo "This script must be run as user node" 1>&2
   exit 1
fi
echo 'create node user ssh key'
mkdir ~/.ssh
chmod 700 ~/.ssh/
ssh-keygen -t rsa -N "" -f /home/node/.ssh/id_rsa
chmod 644 ~/.ssh/id_rsa.pub
chmod 600 ~/.ssh/id_rsa

# install the basic build tools
read -p "Fleet port: " PORT
read -p "Fleet secret: " SECRET

mkdir -p ~/fleet/drone
mkdir -p ~/fleet/hub

# build the startHub script
echo "(cd /home/node/fleet/hub && fleet-hub --port=$PORT --secret=$SECRET)" | tee /home/node/fleet/hub/startHub.sh
echo "(cd /home/node/fleet/drone && fleet-drone --hub=127.0.0.1:$PORT --secret=$SECRET)" | tee /home/node/fleet/drone/startDrone.sh

chmod +x ~/fleet/hub/startHub.sh
chmod +x ~/fleet/drone/startDrone.sh

echo 'creating apps folder'
mkdir -p ~/apps/dispatch/
mkdir -p ~/apps/docparse/
chown -R node:node ~/

echo 'node user setup correctly. You will need to create a DocParse config.json file at /home/node/apps/docparse/config.json before the DocParse services can start'

